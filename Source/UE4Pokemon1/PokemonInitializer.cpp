// Copyright (C) Timo Guehring <timo.guehring@googlemail.com>

#include "UE4Pokemon1.h"
#include "PokemonInitializer.h"

DEFINE_LOG_CATEGORY_STATIC(LogPokemonInitializer, Log, All);

PokemonInitializer* PokemonInitializer::Instance = 0;

PokemonInitializer::PokemonInitializer()
{
	initializePokemon();
}

PokemonInitializer* PokemonInitializer::GetPokemonInitializer()
{
	if (Instance == 0)
		Instance = new PokemonInitializer();
	return Instance;
}

void PokemonInitializer::setAttackAttributes(UAttack* Attack, int32 ID, FString Name, EType Type, int32 PP, int32 Power, int32 Accuracy)
{
	Attack->setID(ID);
	Attack->setName(Name);
	Attack->setType(Type);
	Attack->setPP(PP);
	Attack->setPower(Power);
	Attack->setAccuracy(Accuracy);
}

void PokemonInitializer::setPokemonAttributes(UPokemon* Pokemon,
	int32 ID,
	FString Name,
	int32 Lvl,
	int32 MaxHP,
	int32 CurHP,
	int32 RarFactor,
	FString Region,
	TArray<UAttack*> ListAttack,
	TMap<int32, UAttack*> MapNameAttack,
	float Height,
	float Weight,
	FString Description,
	bool Evolve,
	int32 EvolLvl = 0,
	int32 NextEvolID = 0)
{
	Pokemon->setID(ID);
	Pokemon->setName(Name);
	Pokemon->setLvl(Lvl);
	Pokemon->setMaxHP(MaxHP);
	Pokemon->setCurHP(CurHP);
	Pokemon->setRarFactor(RarFactor);
	Pokemon->setRegion(Region);
	Pokemon->setListAttack(ListAttack);
	Pokemon->setMapNameAttack(MapNameAttack);
	Pokemon->setHeight(Height);
	Pokemon->setWeight(Weight);
	Pokemon->setDescription(Description);
	Pokemon->setEvolve(Evolve);
	Pokemon->setEvolLvl(EvolLvl);
	Pokemon->setNextEvolID(NextEvolID);

	FString Path = FString("/Game/Asset/Textures/Pokemons/");
	Path.operator+=(FString::Printf(TEXT("%03d_"), ID));
	Path.operator+=(Name);

	FString StringBack = FString("");
	StringBack.operator+=(Path);
	StringBack.operator+=(FString::Printf(TEXT("_Back")));
	
	UTexture2D* TextureBack = Cast<UTexture2D>(StaticLoadObject(UTexture2D::StaticClass(), NULL, *StringBack));
	Pokemon->setTextureBack(TextureBack);

	FString StringFront = FString("");
	StringFront.operator+=(Path);
	StringFront.operator+=(FString::Printf(TEXT("_Front")));

	UTexture2D* TextureFront = Cast<UTexture2D>(StaticLoadObject(UTexture2D::StaticClass(), NULL, *StringFront));
	Pokemon->setTextureFront(TextureFront);
}

void PokemonInitializer::initializePokemon()
{

	// http://bulbapedia.bulbagarden.net/wiki/List_of_moves
	//  setAttackAttributes
	//	UAttack*
	//	int32 ID,
	//	FString Name,
	//	ETypeEnum Type,
	//	int32 PP,
	//	int32 Power,
	//	int32 Accuracy

	//---#1 Pound
	UAttack* Pound = NewObject<UAttack>();
	setAttackAttributes(
		Pound,
		1,
		FString("Pound"),
		EType::Normal,
		35,
		40,
		100);

	//---#10 Scratch
	UAttack* Scratch = NewObject<UAttack>(); 
	setAttackAttributes(
		Scratch,
		10,
		FString("Scratch"),
		EType::Normal,
		35,
		40,
		100);

	//---#15 Cut
	UAttack* Cut = NewObject<UAttack>(); 
	setAttackAttributes(
		Cut,
		15,
		FString("Cut"),
		EType::Normal,
		30,
		50,
		95);

	//---#21 Slam
	UAttack* Slam = NewObject<UAttack>(); 
	setAttackAttributes(
		Slam,
		21,
		FString("Slam"),
		EType::Normal,
		20,
		80,
		80);

	//---#22 VineWhip
	UAttack* VineWhip = NewObject<UAttack>(); 
	setAttackAttributes(
		VineWhip,
		22,
		FString("VineWhip"),
		EType::Grass,
		25,
		45,
		100);

	//---#33 Tackle
	UAttack* Tackle = NewObject<UAttack>(); 
	setAttackAttributes(
		Tackle,
		33,
		FString("Tackle"),
		EType::Normal,
		35,
		50,
		100);

	//---#36 TakeDown
	UAttack* TakeDown = NewObject<UAttack>(); 
	setAttackAttributes(
		TakeDown,
		36,
		FString("TakeDown"),
		EType::Normal,
		20,
		90,
		85);

	//---#38 DoubleEdge
	UAttack* DoubleEdge = NewObject<UAttack>(); 
	setAttackAttributes(
		DoubleEdge,
		38,
		FString("DoubleEdge"),
		EType::Normal,
		15,
		120,
		100);

	//---#75 RazorLeaf
	UAttack* RazorLeaf = NewObject<UAttack>(); 
	setAttackAttributes(
		RazorLeaf,
		75,
		FString("RazorLeaf"),
		EType::Grass,
		25,
		55,
		95);


	//  setPokemonAttributes
	//	UPokemon*
	//	int32 ID,
	//	FString Name,
	//	int32 Lvl,
	//	int32 MaxHealth,
	//	int32 CurHealth,
	//	int32 RarFactor,
	//	FString Region,
	//	TArray<Attack*> ListAttack,
	//	TMap<int32, Attack*> MapNameAttack,
	//	float Height,
	//	float Weight,
	//	FString Description,
	//	bool Evolve,
	//	int32 EvolLvl,
	//	int32 NextEvolID

	
	//---#1 Bulbasaur
	TArray<UAttack*> AttacksBulbasaur;
	AttacksBulbasaur.Add(Tackle);

	TMap<int32, UAttack*> MapNameAttackBulbasaur;
	MapNameAttackBulbasaur.Add(9, VineWhip);
	MapNameAttackBulbasaur.Add(15, TakeDown);
	MapNameAttackBulbasaur.Add(19, RazorLeaf);
	MapNameAttackBulbasaur.Add(27, DoubleEdge);

	UPokemon* Bulbasaur = NewObject<UPokemon>();
	setPokemonAttributes(
		Bulbasaur,
		1,
		FString("Bulbasaur"),
		1,
		20,
		20,
		3,
		FString("None"),
		AttacksBulbasaur,
		MapNameAttackBulbasaur,
		0.7f,
		6.9f,
		FString("Bulbasaur desc"),
		true,
		16,
		2);
	ListPokemon.AddUnique(Bulbasaur);

	//---#2 Ivysaur
	TArray<UAttack*> AttacksIvysaur;
	AttacksIvysaur.Add(Tackle);

	TMap<int32, UAttack*> MapNameAttackIvysaur;
	MapNameAttackIvysaur.Add(9, VineWhip);
	MapNameAttackIvysaur.Add(15, TakeDown);
	MapNameAttackIvysaur.Add(20, RazorLeaf);
	MapNameAttackIvysaur.Add(31, DoubleEdge);

	UPokemon* Ivysaur = NewObject<UPokemon>();
	setPokemonAttributes(
		Ivysaur,
		2,
		FString("Ivysaur"),
		20,
		30,
		30,
		3,
		FString("None"),
		AttacksIvysaur,
		MapNameAttackIvysaur,
		1.0f,
		13.0f,
		FString("Ivysaur desc"),
		true,
		32,
		3);
	ListPokemon.AddUnique(Ivysaur);


	//---#3 Venusaur
	TArray<UAttack*> AttacksVenusaur;
	AttacksVenusaur.Add(Tackle);

	TMap<int32, UAttack*> MapNameAttackVenusaur;
	MapNameAttackVenusaur.Add(9, VineWhip);
	MapNameAttackVenusaur.Add(15, TakeDown);
	MapNameAttackVenusaur.Add(20, RazorLeaf);
	MapNameAttackVenusaur.Add(31, DoubleEdge);

	UPokemon* Venusaur = NewObject<UPokemon>();
	setPokemonAttributes(
		Venusaur,
		3,
		FString("Venusaur"),
		30,
		120,
		120,
		3,
		FString("None"),
		AttacksVenusaur,
		MapNameAttackVenusaur,
		2.0f,
		100.0f,
		FString("Venusaur desc"),
		false);
	ListPokemon.AddUnique(Venusaur);

}
