// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//#include "BaseItem.h"
#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/Components/ScrollBox.h"
#include "Runtime/UMG/Public/Components/WrapBox.h"
#include "WrapBoxImpl.h"
#include "BaseItem.h"
#include "PokeInventoryUserWidget.generated.h"

/**
 * 
 */
UCLASS()
class UE4POKEMON1_API UPokeInventoryUserWidget : public UUserWidget
{
	GENERATED_BODY()

	virtual void Construct_Implementation() ;

	virtual void ModifyWrapBox(TSharedPtr<class SWrapBox>);

	virtual TSharedRef<SWidget> GetWrapBox();

	static void OnMenuEntryClicked(int32 ActionIndex);
	
	//virtual TSharedPtr<ISlateStyle> CreateStyle();

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "My New User Widget")
	FString MyNewWidgetName;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ItemArray")
	//TArray<BaseItem> lBaseItem;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ItemArrayFString")
	TArray<FString> lBaseItemFString;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ScrollBoxItems")
	UScrollBox* ScrollBox;

	class SWrapBox* WrapBox;

	class SWidget* WrapBoxMenu;

	FSimpleSlot GetSlot();

	//UFUNCTION()
	//UFUNCTION(BlueprintNativeEvent, BlueprintCosmetic, Category = "User Interface", meta = (Keywords = "Begin Play"))
	//void Construct() override;

	//void Initialize();

	// virtual void PostInitProperties();

	// Beere x 4
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ItemArrayCount")
	//TArray<integer> lBaseItemCount;
	
};
