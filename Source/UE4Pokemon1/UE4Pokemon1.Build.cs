// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class UE4Pokemon1 : ModuleRules
{
	public UE4Pokemon1(TargetInfo Target)
	{
        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "Paper2D", "UMG", "Slate", "SlateCore" });
        //PublicDependencyModuleNames.AddRange(new string[] { "SQLiteSupport" });
	}
}
