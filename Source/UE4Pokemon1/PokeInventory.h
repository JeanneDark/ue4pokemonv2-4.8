// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//#include "BaseItem.h"
#include "Pokeball.h"
#include "Superball.h"
#include "Hyperball.h"
#include "Masterball.h"
#include "Potion.h"
#include "SuperPotion.h"
#include "HyperPotion.h"


class UE4POKEMON1_API PokeInventory : public BaseItemVisitor
{
	
private:

	// TODO unsigned?
	signed int countPokeball = 0;
	signed int countSuperball = 0;
	signed int countHyperball = 1;
	signed int countMasterball = 0;
	signed int countPotion = 0;
	signed int countSuperpotion = 0;
	signed int countHyperpotion = 3;

	TArray<BaseItem*>	listItems;
	TArray<FString>		listDisplayText;

	virtual void SetListItems(TArray<BaseItem*> NewListItems);
	virtual void SetListDisplayText(TArray<FString> NewListDisplayText);

	virtual void UpdateInventory();

public:	

	PokeInventory();
	
	void AddToInventory(BaseItem* Item, class AActor* Actor);
	void AddToInventory(Pokeball Item);
	void AddToInventory(Superball Item);
	void AddToInventory(Hyperball Item);
	void AddToInventory(Masterball Item);
	void AddToInventory(Potion Item);
	void AddToInventory(SuperPotion Item);
	void AddToInventory(HyperPotion Item);

	virtual bool CanAddItem();

	//////////////////////////////////////////////////////////////////////////
	////////// Called for BP events, first called by PokeCharacter

	virtual TArray<BaseItem*> GetListItems();

	virtual TArray<FString> GetListDisplayText();

	//virtual void OnMenuEntryClicked(int32 ActionIndex, UUserWidget* OptionMenuWidget);
	virtual void OnMenuEntryClicked(int32 ActionIndex);

	//////////////////////////////////////////////////////////////////////////
	////////// visit implementations of BaseItemVisitor

	void visit(class BaseItem &);

	void visit(class BaseBall &);
	void visit(class Pokeball &);
	void visit(class Superball &);
	void visit(class Hyperball &);
	void visit(class Masterball &);

	void visit(class BasePotion &);
	void visit(class Potion &);
	void visit(class SuperPotion &);
	void visit(class HyperPotion &);

};
