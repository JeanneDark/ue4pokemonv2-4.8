// Copyright (C) Timo Guehring <timo.guehring@googlemail.com>

#include "UE4Pokemon1.h"
#include "PokemonManager.h"

UPokemonManager::UPokemonManager()
{
	
}

void UPokemonManager::initialize()
{
	PokemonInitializer* PInitializer = PokemonInitializer::GetPokemonInitializer();
	ListPokemon = PInitializer->GetPokemon();
	// TArray<Attack*> attackList = List.operator [](0)->GetAttacks();

}

UTexture2D* UPokemonManager::getOwnPokemonTexture()
{
	FString Path = FString("/Game/Asset/Textures/Pokemons/001_Bulbasaur_Back");
	UTexture2D* Texture = Cast<UTexture2D>(StaticLoadObject(UTexture2D::StaticClass(), NULL, *Path));

	return Texture;
}

UPokemon* UPokemonManager::getPokemonOwn()
{
	return ListPokemon.operator [](0);
}

UPokemon* UPokemonManager::getPokemonEnemy()
{
	return ListPokemon.operator [](0);
	//return ListPokemon.operator [](1);
}
