// Fill out your copyright notice in the Description page of Project Settings.

#include "UE4Pokemon1.h"
#include "PokeActor.h"
#include "PaperSpriteComponent.h"

// Sets default values
APokeActor::APokeActor(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Setup the assets
	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UPaperSprite> PokeballSpriteAsset;

		FConstructorStatics()
			: PokeballSpriteAsset(TEXT("/Game/Asset/Sprites/Pokeball.Pokeball"))
		{
		}
	};
	static FConstructorStatics ConstructorStatics;

	PokeballSprite = ConstructorStatics.PokeballSpriteAsset.Get();

	// UPaperSprite* PokeballSprite = ConstructorStatics.PokeballSpriteAsset.Get();

	// Box2 = ObjectInitializer.CreateDefaultSubobject<UBoxComponent>(this, TEXT("BoxPokeball"));
	
	USceneComponent* SceneComponent = ObjectInitializer.CreateDefaultSubobject<USceneComponent>(this, TEXT("SceneComp"));
	RootComponent = SceneComponent;
	RootComponent->Mobility = EComponentMobility::Static;

	// UBillboardComponent * SpriteComponent = ObjectInitializer.CreateEditorOnlyDefaultSubobject<UBillboardComponent>(this, TEXT("Sprite"));

	//Box2->bGenerateOverlapEvents = true;
	//RootComponent = Box2;

	//Box2->OnComponentBeginOverlap.AddDynamic(this, &APokeActor::OnOverlapBegin);
	//Box2->OnComponentEndOverlap.AddDynamic(this, &APokeActor::OnOverlapEnd);

	// SpriteComponent = ObjectInitializer.CreateEditorOnlyDefaultSubobject<UBillboardComponent>(this, TEXT("Sprite"));
	PokeballSprite->AddToRoot();

	

}

/*
void APokeActor::OnOverlapBegin_Implementation(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("APokeActor-------OnOverlapBegin-----"));

}

void APokeActor::OnOverlapEnd_Implementation(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	UE_LOG(LogTemp, Warning, TEXT("APokeActor-------OnOverlapExit-----"));

}
*/