// Fill out your copyright notice in the Description page of Project Settings.

#include "UE4Pokemon1.h"
#include "Utility.h"

Utility::Utility()
{
}

int32 Utility::GenerateRandomPaceCount()
{
	return rand() % (MAX_PACE - MIN_PACE + 1) + MIN_PACE;
}

int32 Utility::GenerateRandomNumberBtw0And3()
{
	return rand() % (3 - 0 + 1) + 0;
}
