#pragma once

#include "Map.h"
#include "BaseItem.h"
#include "ItemManager.generated.h"


UENUM(BlueprintType)
enum class EItemEnum : uint8
{
	Trank 				UMETA(DisplayName = "Trank"),
	Pokeball 			UMETA(DisplayName = "Pokeball"),
	Superball			UMETA(DisplayName = "Superball"),
	Meisterball			UMETA(DisplayName = "Meisterball"),
	None				UMETA(DisplayName = "None")
};


UENUM(BlueprintType)
enum class ETypeEnum : uint8
{
	PokeballItem 		UMETA(DisplayName = "PokeballItem"),
	Sign	 			UMETA(DisplayName = "Sign"),
	Door				UMETA(DisplayName = "Door"),
	None				UMETA(DisplayName = "None")
};


UCLASS()
class UItemManager : public UObject
{
	GENERATED_BODY()

public:
	UItemManager(const FObjectInitializer& ObjectInitializer);

	static EItemEnum GetCorrespondingStringAsEItemEnum(const FString String);
	static BaseItem* GetCorrespondingStringAsBaseItem(const FString String);
	//static FString GetCorrespondingEnumAsString(const EItemEnum Item);

	static ETypeEnum GetCorrespondingStringAsETypeEnum(const FString String);
	//static FString GetCorrespondingEnumAsString(const ETypeEnum Item);

};
