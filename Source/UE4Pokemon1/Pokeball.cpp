
#include "UE4Pokemon1.h"
#include "Pokeball.h"


Pokeball::Pokeball()
{
	maxVal = 255;
	price = 200;
	description = FString("Damit f�ngst du wilde Pokemon. Du wirfst ihn wie einen normalen Ball. Das Design �hnelt dem einer Kapsel.");
}

void Pokeball::accept(BaseItemVisitor &v)
{
	v.visit(*this);
}