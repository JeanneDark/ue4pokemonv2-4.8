// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
//#pragma message ("~~~~~~~~~~~~BaseItemVisitor object is defined")

/*
#include "BaseItem.h"
#include "BaseBall.h"
#include "Pokeball.h"
#include "Superball.h"
#include "Hyperball.h"
#include "Masterball.h"
*/


/**
 * 
 */
class BaseItemVisitor
{
public:

	BaseItemVisitor();

	virtual void visit(class BaseItem &) = 0;

	virtual void visit(class BaseBall &) = 0;
	virtual void visit(class Pokeball &) = 0;
	virtual void visit(class Superball &) = 0;
	virtual void visit(class Hyperball &) = 0;
	virtual void visit(class Masterball &) = 0;

	virtual void visit(class BasePotion &) = 0;
	virtual void visit(class Potion &) = 0;
	virtual void visit(class SuperPotion &) = 0;
	virtual void visit(class HyperPotion &) = 0;
	
};
