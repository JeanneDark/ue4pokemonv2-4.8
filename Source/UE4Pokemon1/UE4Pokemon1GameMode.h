// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#pragma once
#include "GameFramework/GameMode.h"
#include "UE4Pokemon1GameMode.generated.h"

// The GameMode defines the game being played. It governs the game rules, scoring, what actors
// are allowed to exist in this game type, and who may enter the game.
//

UCLASS(minimalapi)
class AUE4Pokemon1GameMode : public AGameMode
{
	GENERATED_BODY()
public:
	AUE4Pokemon1GameMode(const FObjectInitializer& ObjectInitializer);
};
