// Copyright (C) Timo Guehring <timo.guehring@googlemail.com>

#pragma once

#include "BaseActor.h"
#include "GameFramework/Actor.h"
#include "CommonActor.generated.h"

UCLASS()
class UE4POKEMON1_API ACommonActor : public ABaseActor
{
	GENERATED_BODY()
	
	virtual void SetupAssets();

public:	
	ACommonActor(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;
	
};
