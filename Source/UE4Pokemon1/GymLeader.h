// Copyright (C) Timo Guehring <timo.guehring@googlemail.com>

#pragma once

#include "BaseActor.h"
#include "GameFramework/Actor.h"
#include "GymLeader.generated.h"

/**
 *
 */
UCLASS()
class UE4POKEMON1_API AGymLeader : public ABaseActor
{
	GENERATED_BODY()

	virtual void SetupAssets();

public:
	AGymLeader(const FObjectInitializer& ObjectInitializer);

	virtual void Tick(float DeltaSeconds) override;

};
