// Fill out your copyright notice in the Description page of Project Settings.

#include "UE4Pokemon1.h"
#include "BaseItem.h"
#include "Potion.h"
#include "SuperPotion.h"
#include "HyperPotion.h"
#include "Pokeball.h"
#include "Superball.h"
#include "Hyperball.h"
#include "Masterball.h"
#include "ItemManager.h"


static TMap<FString, EItemEnum> MapNameEItemEnum;
static TMap<FString, BaseItem*>	MapNameBaseItem;
static TMap<FString, ETypeEnum> MapNameETypeEnum;

static void init()
{
	MapNameEItemEnum.Add(FString(TEXT("Trank")), EItemEnum::Trank);
	MapNameEItemEnum.Add(FString(TEXT("Pokeball")), EItemEnum::Pokeball);
	MapNameEItemEnum.Add(FString(TEXT("Superball")), EItemEnum::Superball);
	MapNameEItemEnum.Add(FString(TEXT("Meisterball")), EItemEnum::Meisterball);
	MapNameEItemEnum.Add(FString(TEXT("None")), EItemEnum::None);

	MapNameBaseItem.Add(FString(TEXT("Trank")), new Potion());
	MapNameBaseItem.Add(FString(TEXT("Supertrank")), new SuperPotion());
	MapNameBaseItem.Add(FString(TEXT("Hypertrank")), new HyperPotion());
	MapNameBaseItem.Add(FString(TEXT("Pokeball")), new Pokeball());
	MapNameBaseItem.Add(FString(TEXT("Superball")), new Superball());
	MapNameBaseItem.Add(FString(TEXT("Hyperball")), new Hyperball());
	MapNameBaseItem.Add(FString(TEXT("Meisterball")), new Masterball());

	MapNameETypeEnum.Add(FString(TEXT("PokeballItem")), ETypeEnum::PokeballItem);
	MapNameETypeEnum.Add(FString(TEXT("Sign")), ETypeEnum::Sign);
	MapNameETypeEnum.Add(FString(TEXT("Door")), ETypeEnum::Door);
	MapNameETypeEnum.Add(FString(TEXT("None")), ETypeEnum::None);

}

UItemManager::UItemManager(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	init();
}

BaseItem* UItemManager::GetCorrespondingStringAsBaseItem(const FString Str)
{
	BaseItem** Item = MapNameBaseItem.Find(Str);

	if (Item)
		return *Item;
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UItemManager-------GetCorrespondingStringAsBaseItem-----NULL"));
		return nullptr;
		//UE_LOG(LogTemp, Warning, TEXT("UItemManager-------GetCorrespondingStringAsBaseItem-----NULL"));
		//return BaseItem();
	}

}

EItemEnum UItemManager::GetCorrespondingStringAsEItemEnum(const FString Str)
{
	EItemEnum* Item = MapNameEItemEnum.Find(Str);

	if (Item)
		return *Item;
	else
		return EItemEnum::None;

}

ETypeEnum UItemManager::GetCorrespondingStringAsETypeEnum(const FString Str)
{
	ETypeEnum* Item = MapNameETypeEnum.Find(Str);

	if (Item)
		return *Item;
	else
		return ETypeEnum::None;

}

/*
FString UItemManager::GetCorrespondingEnumAsString(const ETypeEnum item)
{
	const UEnum* EnumPtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("ETypeEnum"), true);
	FText ftext = EnumPtr->GetDisplayNameText((int32)item);

	return ftext.ToString();

}

FString UItemManager::GetCorrespondingEnumAsString(const EItemEnum item)
{
	const UEnum* EnumPtr = FindObject<UEnum>(ANY_PACKAGE, TEXT("EItemEnum"), true);
	FText ftext = EnumPtr->GetDisplayNameText((int32)item);
	
	return ftext.ToString();

}
*/