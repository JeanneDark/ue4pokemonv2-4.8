// Copyright (C) Timo Guehring <timo.guehring@googlemail.com>

#include "UE4Pokemon1.h"
#include "GymLeader.h"



AGymLeader::AGymLeader(const FObjectInitializer& ObjectInitializer)
	: ABaseActor(ObjectInitializer)
{
	SetupAssets();
	SetupActor();
}

// TODO pass e.g. "blue" as a tag and then load path + param as assets
void AGymLeader::SetupAssets()
{
	UE_LOG(LogTemp, Warning, TEXT("AGymLeader------SetupAssets"));
	TArray<FString> Data = GetActorTags();

	TArray<FName> Tags = Flipbook->ComponentTags;

	UE_LOG(Poke, Warning, TEXT("AGymLeader-------Tagnum: %d "), Tags.Num());

	FString Path = FString("/Game/Asset/Animations/Figures/GymLeader/");

	for (int i = 1; i < Data.Num(); i++)
	{
		UPaperFlipbook* TmpFlp = *MapDirectionAnimation.Find(Data[i]);

		if (TmpFlp)
		{
			TmpFlp = (UPaperFlipbook*) (UPaperFlipbook::StaticClass(), NULL, *Path);

			UE_LOG(LogTemp, Warning, TEXT("AGymLeader------SetupAssets-TmpFlp"));
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("AGymLeader-SetupAssets-Invalid Tags"));
		}

	}


	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> RunningDownAnimationAsset;
		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> RunningUpAnimationAsset;
		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> RunningRightAnimationAsset;
		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> RunningLeftAnimationAsset;

		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> IdleDownAnimationAsset;
		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> IdleUpAnimationAsset;
		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> IdleRightAnimationAsset;
		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> IdleLeftAnimationAsset;

		FConstructorStatics()

			: RunningDownAnimationAsset(TEXT("/Game/Asset/Animations/Figures/Others/BlueDown.BlueDown"))
			, RunningUpAnimationAsset(TEXT("/Game/Asset/Animations/Figures/Others/BlueUp.BlueUp"))
			, RunningRightAnimationAsset(TEXT("/Game/Asset/Animations/Figures/Others/BlueRight.BlueRight"))
			, RunningLeftAnimationAsset(TEXT("/Game/Asset/Animations/Figures/Others/BlueLeft.BlueLeft"))

			, IdleDownAnimationAsset(TEXT("/Game/Asset/Animations/Figures/Others/BlueDownIdle.BlueDownIdle"))
			, IdleUpAnimationAsset(TEXT("/Game/Asset/Animations/Figures/Others/BlueUpIdle.BlueUpIdle"))
			, IdleRightAnimationAsset(TEXT("/Game/Asset/Animations/Figures/Others/BlueRightIdle.BlueRightIdle"))
			, IdleLeftAnimationAsset(TEXT("/Game/Asset/Animations/Figures/Others/BlueLeftIdle.BlueLeftIdle"))

		{
		}
	};
	static FConstructorStatics ConstructorStatics;

	RunningDownAnimation = ConstructorStatics.RunningDownAnimationAsset.Get();
	RunningUpAnimation = ConstructorStatics.RunningUpAnimationAsset.Get();
	RunningRightAnimation = ConstructorStatics.RunningRightAnimationAsset.Get();
	RunningLeftAnimation = ConstructorStatics.RunningLeftAnimationAsset.Get();

	IdleDownAnimation = ConstructorStatics.IdleDownAnimationAsset.Get();
	IdleUpAnimation = ConstructorStatics.IdleUpAnimationAsset.Get();
	IdleRightAnimation = ConstructorStatics.IdleRightAnimationAsset.Get();
	IdleLeftAnimation = ConstructorStatics.IdleLeftAnimationAsset.Get();

}

void AGymLeader::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	IsHittingCharacter();
}


