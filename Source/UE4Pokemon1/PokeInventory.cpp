// Fill out your copyright notice in the Description page of Project Settings.

#include "UE4Pokemon1.h"
#include "PokeInventory.h"

/**
 *
 * TODO:
 * - SQLite DB initializing with data 
 * - MaxItems
 * - Show message when picked up
 */
PokeInventory::PokeInventory()
{
	// TODO Save mechanism

	UpdateInventory();
}

// TODO more elegant
void PokeInventory::UpdateInventory()
{
	TArray<BaseItem*> lItems;
	TArray<FString> lDisyplayText;

	if (countPokeball > 0)
	{
		lItems.Add(new Pokeball());
		lDisyplayText.Add(FString::Printf(TEXT("Pokeball x %d"), countPokeball));
		UE_LOG(LogTemp, Warning, TEXT("-PokeInventory::UpdateInventory()-"));
	}
	if (countSuperball > 0)
	{
		lItems.Add(new Superball());
		lDisyplayText.Add(FString::Printf(TEXT("Superball x %d"), countSuperball));
	}
	if (countHyperball > 0)
	{
		lItems.Add(new Hyperball());
		lDisyplayText.Add(FString::Printf(TEXT("Hyperball x %d"), countHyperball));
		UE_LOG(LogTemp, Warning, TEXT("Hyperball x "));
	}
	if (countMasterball > 0)
	{
		lItems.Add(new Masterball());
		lDisyplayText.Add(FString::Printf(TEXT("Masterball x %d"), countMasterball));
	}
	if (countPotion > 0)
	{
		lItems.Add(new Potion());
		lDisyplayText.Add(FString::Printf(TEXT("Potion x %d"), countPotion));
	}
	if (countSuperpotion > 0)
	{
		lItems.Add(new SuperPotion());
		lDisyplayText.Add(FString::Printf(TEXT("Superotion x %d"), countSuperpotion));
	}
	if (countHyperpotion > 0)
	{
		lItems.Add(new HyperPotion());
		lDisyplayText.Add(FString::Printf(TEXT("Hyperpotion x %d"), countHyperpotion));
	}

	SetListItems(lItems);
	SetListDisplayText(lDisyplayText);

}


void PokeInventory::visit(BaseItem& Item)
{
	UE_LOG(LogTemp, Warning, TEXT("PokeInventory---visit----BaseItem-----"));
}

void PokeInventory::visit(BaseBall& Item)
{
	UE_LOG(LogTemp, Warning, TEXT("PokeInventory---visit----BaseBall-----"));
}

void PokeInventory::visit(Pokeball &Item)
{
	UE_LOG(LogTemp, Warning, TEXT("PokeInventory---visit----Pokeball-----"));
	AddToInventory(Item);
}

void PokeInventory::visit(Superball &Item)
{
	UE_LOG(LogTemp, Warning, TEXT("PokeInventory---visit----Superball-----"));
	AddToInventory(Item);
}

void PokeInventory::visit(Hyperball	&Item)
{
	UE_LOG(LogTemp, Warning, TEXT("PokeInventory---visit----Hyperball-----"));
	AddToInventory(Item);
}

void PokeInventory::visit(Masterball& Item)
{
	UE_LOG(LogTemp, Warning, TEXT("PokeInventory---visit----Masterball-----"));
	AddToInventory(Item);
}

void PokeInventory::visit(BasePotion& Item)
{
	UE_LOG(LogTemp, Warning, TEXT("PokeInventory---visit----BasePotion-----"));
}

void PokeInventory::visit(Potion& Item)
{
	UE_LOG(LogTemp, Warning, TEXT("PokeInventory---visit----Potion-----"));
	AddToInventory(Item);
}

void PokeInventory::visit(SuperPotion& Item)
{
	UE_LOG(LogTemp, Warning, TEXT("PokeInventory---visit----SuperPotion-----"));
	AddToInventory(Item);
}

void PokeInventory::visit(HyperPotion& Item)
{
	UE_LOG(LogTemp, Warning, TEXT("PokeInventory---visit----HyperPotion----+++"));
	AddToInventory(Item);
}


void PokeInventory::AddToInventory(BaseItem* Item, class AActor* Actor)
{
	if (Item)
	{
		if (CanAddItem())
		{
			//UE_LOG(LogTemp, Warning, TEXT("PokeInventory---AddToInventory----BaseItem-----"));
			Item->accept(*this);
			Actor->Destroy();
		}
		else
		{
			// TODO Implement else
			// show message
		}
		
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("PokeInventory---AddToInventory----NULL-----"));
	}
}

void PokeInventory::AddToInventory(Pokeball Item)
{
	UE_LOG(LogTemp, Warning, TEXT("PokeInventory---AddToInventory----Pokeball-----"));
	countPokeball++;
	UpdateInventory();
}

void PokeInventory::AddToInventory(Superball Item)
{
	UE_LOG(LogTemp, Warning, TEXT("PokeInventory---AddToInventory----Superball-----"));
	countSuperball++;
	UpdateInventory();
}

void PokeInventory::AddToInventory(Hyperball Item)
{
	UE_LOG(LogTemp, Warning, TEXT("PokeInventory---AddToInventory----Hyperball-----"));
	countHyperball++;
	UpdateInventory();
}

void PokeInventory::AddToInventory(Masterball Item)
{
	UE_LOG(LogTemp, Warning, TEXT("PokeInventory---AddToInventory----Masterball-----"));
	countMasterball++;
	UpdateInventory();
}

void PokeInventory::AddToInventory(Potion Item)
{
	UE_LOG(LogTemp, Warning, TEXT("PokeInventory---AddToInventory----Potion-----"));
	countPotion++;
	UpdateInventory();
}

void PokeInventory::AddToInventory(SuperPotion Item)
{
	UE_LOG(LogTemp, Warning, TEXT("PokeInventory---AddToInventory----SuperPotion-----"));
	countSuperpotion++;
	UpdateInventory();
}

void PokeInventory::AddToInventory(HyperPotion Item)
{
	UE_LOG(LogTemp, Warning, TEXT("PokeInventory---AddToInventory----HyperPotion-----"));
	countHyperpotion++;
	UpdateInventory();
}


void PokeInventory::SetListItems(TArray<BaseItem*> NewListItems)
{
	listItems = NewListItems;
}

void PokeInventory::SetListDisplayText(TArray<FString> NewListDisplayText)
{
	listDisplayText = NewListDisplayText;
}

TArray<BaseItem*> PokeInventory::GetListItems()
{
	return listItems;
}

TArray<FString> PokeInventory::GetListDisplayText()
{
	return listDisplayText;
}

void PokeInventory::OnMenuEntryClicked(int32 ActionIndex)
{
	UE_LOG(LogTemp, Warning, TEXT("UPokeInventoryUserWidget-------OnMenuEntryClicked-----"));

}

/*
void PokeInventory::OnMenuEntryClicked(int32 ActionIndex, UUserWidget* OptionMenuWidget)
{
	UE_LOG(LogTemp, Warning, TEXT("UPokeInventoryUserWidget-------OnMenuEntryClicked-----"));

	if (ActionIndex >= GetListDisplayText().Num())
	{
		OptionMenuWidget->RemoveFromViewport();
	}

}
*/

bool PokeInventory::CanAddItem() { return true; }

