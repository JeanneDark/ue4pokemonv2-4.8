// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 * 
 */
class UE4POKEMON1_API SInventoryWidget : public SCompoundWidget
{


public:
	SLATE_BEGIN_ARGS(SInventoryWidget)
	{
	}
	SLATE_ARGUMENT(TWeakObjectPtr<class AInventoryHUD>, InventoryHUD)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	// Click handler for the Play Game! button � Calls MenuHUD�s PlayGameClicked() event.
	FReply PlayGameClicked();

	// Click handler for the Quit Game button � Calls MenuHUD�s QuitGameClicked() event.
	FReply QuitGameClicked();

	// Stores a weak reference to the HUD controlling this class.
	TWeakObjectPtr<class AInventoryHUD> InventoryHUD;

};
