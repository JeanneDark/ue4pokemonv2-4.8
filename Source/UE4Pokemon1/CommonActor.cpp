// Copyright (C) Timo Guehring <timo.guehring@googlemail.com>

#include "UE4Pokemon1.h"
#include "CommonActor.h"


// Sets default values
ACommonActor::ACommonActor(const FObjectInitializer& ObjectInitializer)
	: ABaseActor(ObjectInitializer)
{
	SetupAssets();
	SetupActor();

}

// TODO pass e.g. "blue" as a tag and then load path + param as assets
void ACommonActor::SetupAssets()
{

	UE_LOG(LogTemp, Warning, TEXT("ACommonActor------SetupAssets"));

	TArray<FName> Tags = Box2->ComponentTags;

	UE_LOG(Poke, Warning, TEXT("ACommonActor-------Tagnum: %d "), Tags.Num());

	// Setup the assets
	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> RunningDownAnimationAsset;
		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> RunningUpAnimationAsset;
		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> RunningRightAnimationAsset;
		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> RunningLeftAnimationAsset;

		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> IdleDownAnimationAsset;
		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> IdleUpAnimationAsset;
		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> IdleRightAnimationAsset;
		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> IdleLeftAnimationAsset;

		FConstructorStatics()

			: RunningDownAnimationAsset(TEXT("/Game/Asset/Animations/Figures/Others/BlueDown.BlueDown"))
			, RunningUpAnimationAsset(TEXT("/Game/Asset/Animations/Figures/Others/BlueUp.BlueUp"))
			, RunningRightAnimationAsset(TEXT("/Game/Asset/Animations/Figures/Others/BlueRight.BlueRight"))
			, RunningLeftAnimationAsset(TEXT("/Game/Asset/Animations/Figures/Others/BlueLeft.BlueLeft"))

			, IdleDownAnimationAsset(TEXT("/Game/Asset/Animations/Figures/Others/BlueDownIdle.BlueDownIdle"))
			, IdleUpAnimationAsset(TEXT("/Game/Asset/Animations/Figures/Others/BlueUpIdle.BlueUpIdle"))
			, IdleRightAnimationAsset(TEXT("/Game/Asset/Animations/Figures/Others/BlueRightIdle.BlueRightIdle"))
			, IdleLeftAnimationAsset(TEXT("/Game/Asset/Animations/Figures/Others/BlueLeftIdle.BlueLeftIdle"))

		{
		}
	};
	static FConstructorStatics ConstructorStatics;

	RunningDownAnimation = ConstructorStatics.RunningDownAnimationAsset.Get();
	RunningUpAnimation = ConstructorStatics.RunningUpAnimationAsset.Get();
	RunningRightAnimation = ConstructorStatics.RunningRightAnimationAsset.Get();
	RunningLeftAnimation = ConstructorStatics.RunningLeftAnimationAsset.Get();

	IdleDownAnimation = ConstructorStatics.IdleDownAnimationAsset.Get();
	IdleUpAnimation = ConstructorStatics.IdleUpAnimationAsset.Get();
	IdleRightAnimation = ConstructorStatics.IdleRightAnimationAsset.Get();
	IdleLeftAnimation = ConstructorStatics.IdleLeftAnimationAsset.Get();

}

// TODO maybe setlocation() in beginplay()
void ACommonActor::BeginPlay()
{
	Super::BeginPlay();

	CreateTimer();
}

void ACommonActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	IsHittingCharacter();
}

