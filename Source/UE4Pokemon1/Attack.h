// Copyright (C) Timo Guehring <timo.guehring@googlemail.com>

#pragma once

// #include "Pokemon.h"


// #ifndef Pokemon_H_
// #define Pokemon_H_

#include "Attack.generated.h"

enum class EType : uint8;

/**
 * 
 * TODO:
 * - Status attacks
 * - Max PP
 *
 */
UCLASS()
class UAttack : public UObject
{

private:

	GENERATED_BODY()

	int32 ID;
	FString Name;
	EType Type;					// determines the type e.g. water, fire, etc.
	int32 PP;					// PP = PowerPoints (AP in German)
	int32 Power;
	int32 Accuracy;
	
public:
	// Has to offer a constructor without parameters because of the UObject
	UAttack();

	//////////////////////////////////////////////////////////////////////////
	////////// Getter

	int32 getID() { return ID; };
	FString getName() { return Name; };
	UFUNCTION(BlueprintCallable, Category = "Pokemon")
	FText getNameFText() { return FText::FromString(Name); };
	EType getType() { return Type; };
	int32 getPP() { return PP; };
	int32 getPower() { return Power; };
	int32 getAccuracy() { return Accuracy; };


	/////////////////////////////////////////////////////////////////////////
	////////// Setter
	
	void setID(int32 ID) { this->ID = ID; }
	void setName(FString Name) { this->Name = Name; }
	void setType(EType Type) { this->Type = Type; }
	void setPP(int32 PP) { this->PP = PP; };
	void setPower(int32 Power) { this->Power = Power; }
	void setAccuracy(int32 Accuracy) { this->Accuracy = Accuracy; }

};
// #endif