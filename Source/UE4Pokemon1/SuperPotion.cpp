
#include "UE4Pokemon1.h"
#include "SuperPotion.h"


SuperPotion::SuperPotion()
{
	price = 700;
	priceOfSale = 350;
	countOfHP = 50;
	description = FString("SuperTrank");
}

void SuperPotion::accept(BaseItemVisitor &v)
{
	v.visit(*this);
}