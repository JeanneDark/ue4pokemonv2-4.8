// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "UE4Pokemon1.h"
#include "UE4Pokemon1GameMode.h"
#include "PokeCharacter.h"
#include "InventoryHUD.h"

AUE4Pokemon1GameMode::AUE4Pokemon1GameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// set default pawn class to our character
	DefaultPawnClass = APokeCharacter::StaticClass();	
	//HUDClass = AInventoryHUD::StaticClass();
}
