// Fill out your copyright notice in the Description page of Project Settings.

#include "UE4Pokemon1.h"
#include "PokeInventoryUserWidget.h"

/**
 * Check if RebuildWidget() isnt the proper method to build the menu
 * http://www.barisatamer.com/button-with-release-event/
 *
 *
 * TODO https://wiki.unrealengine.com/Slate_Tabs
 */
void UPokeInventoryUserWidget::Construct_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("UPokeInventoryUserWidget-------Construct-----"));

	UWidget* uwidget = GetRootWidget();
	UWrapBoxImpl* uwrapbox = (UWrapBoxImpl*) GetWidgetFromName(TEXT("WrapBox_Test"));
	TSharedPtr<class SWrapBox> WrapBox = uwrapbox->GetMyWrapBox();
	
	TSharedRef<SWidget> WrapBoxMenu = GetWrapBox();

	WrapBox->AddSlot()[WrapBoxMenu];

}

TSharedRef<SWidget> UPokeInventoryUserWidget::GetWrapBox()
{
	// Create menu builder. Note that you dont have to pass optional parameters!
	FMenuBuilder MenuBuilder(true, NULL);

	//TSharedPtr<ISlateStyle> InStyleSet = CreateStyle();

	// Create Action container that will be used for callbacks
	FUIAction Action;
	
	//MenuBuilder.SetStyle(InStyleSet.Get(), FName(TEXT("Menu.Label")));

	FTextBlockStyle* TextBlockStyle = new FTextBlockStyle();
	TextBlockStyle->SetFontSize(20);
	TextBlockStyle->SetFont(FSlateFontInfo("Arial", 26));

	TSharedPtr<class SWidget> swidget = SNew(STextBlock)
		.TextStyle(TextBlockStyle)
		.Text(FText::FromString("Label here..."))
		.ShadowOffset(FIntPoint(-1, 1))
		.MinDesiredWidth(300.0f);

	MenuBuilder.BeginSection("TestSection");
	{

		// 1st entry should be a label, name or whatever
		MenuBuilder.AddWidget(
			SNew(STextBlock)
			.TextStyle(TextBlockStyle)
			.Text(FText::FromString("Label here..."))
			.ShadowOffset(FIntPoint(-1, 1))
			.MinDesiredWidth(300.0f),
			FText(),
			true);

		for (int32 i = 0; i < 5; i++)
		{
			// For each entry create new action that will pass coresponding index
			Action = FUIAction(FExecuteAction::CreateStatic(&UPokeInventoryUserWidget::OnMenuEntryClicked, i));

			// #1 works
			MenuBuilder.AddMenuEntry(
				FText::FromString(FString::Printf(TEXT("Entry no. %d"), i)),
				FText(),
				FSlateIcon(),
				Action);

			// #2 doesnt work
			// AddMenuEntry(
			//	const FUIAction& UIAction,
			//	const TSharedRef< SWidget > Contents,
			//	const FName& InExtensionHook,
			//	const TAttribute<FText>& InToolTip,
			//	const EUserInterfaceActionType::Type UserInterfaceActionType,
			//	FName InTutorialHighlightName )
	
			/*
			FName* name = new FName();
			//FText* ftext = new FText();
			MenuBuilder.AddMenuEntry(
				Action,
				swidget,
				FName(),
				FText(),
				EUserInterfaceActionType::Button,
				*name
				);
			*/
		}
	}
	MenuBuilder.EndSection();

	return MenuBuilder.MakeWidget();
}

/*
TSharedPtr<ISlateStyle> UPokeInventoryUserWidget::CreateStyle()
{
	TSharedPtr<FSlateStyleSet> Style = MakeShareable(new FSlateStyleSet("PreloadStyle"));
	Style->SetContentRoot(FPaths::GameContentDir() / "Slate");

	FTextBlockStyle TextBlockStyle = FTextBlockStyle();
	TextBlockStyle.SetFontSize(20);
	TextBlockStyle.SetFont(FSlateFontInfo("Arial", 26));

	//Style->Set("tab_normal", new IMAGE_BRUSH("tab_normal", FVector2D(256, 64)));
	//Style->Set("tab_active", new IMAGE_BRUSH("tab_active", FVector2D(256, 64)));
	//Style->Set("Menu.Background", new BOX_BRUSH("Old/Menu_Background", FMargin(8.0f / 64.0f)));
	Style->Set("Menu.Label", TextBlockStyle);

	return Style;
}
*/

void UPokeInventoryUserWidget::ModifyWrapBox(TSharedPtr<class SWrapBox> swrapbox)
{
	// Create Action container that will be used for callbacks
	//FUIAction Action;

	for (int32 i = 0; i < 5; i++)
	{
		// For each entry create new action that will pass coresponding index
		//Action = FUIAction(FExecuteAction::CreateStatic(&UPokeInventoryUserWidget::OnMenuEntryClicked, i));

		swrapbox->AddSlot()[
			SNew(SWrapBox)
				.PreferredWidth(300.f)
				+ SWrapBox::Slot()
				.Padding(5)
				.VAlign(VAlign_Top)
				[
					SNew(SButton)
					//.ColorAndOpacity(FLinearColor::White)
					//.ShadowColorAndOpacity(FLinearColor::Black)
					//.ShadowOffset(FIntPoint(-1, 1))
					//.Font(FSlateFontInfo("Arial", 26))
					.Text(FText::FromString("Main Menu1"))
				]
		];
	}
	
}

void UPokeInventoryUserWidget::OnMenuEntryClicked(int32 ActionIndex)
{
	UE_LOG(LogTemp, Warning, TEXT("UPokeInventoryUserWidget-------OnMenuEntryClicked-----"));
}