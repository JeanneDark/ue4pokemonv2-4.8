// Copyright (C) Timo Guehring <timo.guehring@googlemail.com>

#pragma once

// #ifndef Pokemon_H_
// #define Pokemon_H_

#include "Attack.h"
#include "Pokemon.generated.h"

#define LOCTEXT_NAMESPACE "Pokemon" 
// class UAttack;

enum class EType : uint8
{
	Normal 			UMETA(DisplayName = "Normal"),
	Fight	 		UMETA(DisplayName = "Fight"),
	Flying			UMETA(DisplayName = "Flying"),
	Poision			UMETA(DisplayName = "Poision"),
	Ground			UMETA(DisplayName = "Ground"),
	Rock			UMETA(DisplayName = "Rock"),
	Bug				UMETA(DisplayName = "Bug"),
	Ghost			UMETA(DisplayName = "Ghost"),
	Steel			UMETA(DisplayName = "Steel"),
	Fire			UMETA(DisplayName = "Fire"),
	Water			UMETA(DisplayName = "Water"),
	Grass			UMETA(DisplayName = "Grass"),
	Electric		UMETA(DisplayName = "Electric"),
	Psychic			UMETA(DisplayName = "Psychic"),
	Ice				UMETA(DisplayName = "Ice"),
	Dragon			UMETA(DisplayName = "Dragon"),
	Dark			UMETA(DisplayName = "Dark"),
	Fairy			UMETA(DisplayName = "Fairy")
};

/**
 * 
 */
UCLASS()
class UPokemon : public UObject
{

private:

	GENERATED_BODY()

	// TODO maybe member bool own
	// TODO implement logic to determine the needed exp for lvl up
	// TODO List with enum types fire, water, etc

	// Basics
	int32 ID;
	FString Name;
	int32 Lvl;				// level
	UTexture2D* TextureBack;
	UTexture2D* TextureFront;

	int32 Exp = 0;			// experience
	int32 MaxHP;			// maximal Health Points
	int32 CurHP;			// current Health Points
	int32 RarFactor;		// rarity factor
	FString Region;

	// Attacks
	TArray<UAttack*> ListAttack;
	TMap<int32, UAttack*> MapNameAttack;	// HashMap with <int lvl, attack> he can learn

	// Pokedex data
	float Height;
	float Weight;
	FString Description;

	// Evolution
	bool Evolve;			// determines whether a Pokemon can evolve
	int32 EvolLvl;			// determines at which level the Pokemon evolves
	int32 NextEvolID;

public:
	// Has to offer a constructor without parameters because of the UObject
	UPokemon();

	//////////////////////////////////////////////////////////////////////////
	////////// Getter

	// Basics
	UFUNCTION(BlueprintCallable, Category = "Pokemon")
	int32 getID() { return ID; };
	UFUNCTION(BlueprintCallable, Category = "Pokemon")
	FString getName() { return Name; };
	UFUNCTION(BlueprintCallable, Category = "Pokemon")
	FText getNameFText() { return FText::FromString(Name); };
	UFUNCTION(BlueprintCallable, Category = "Pokemon")
	int32 getLvl() { return Lvl; };
	UFUNCTION(BlueprintCallable, Category = "Pokemon")
	UTexture2D* getTextureBack() { return TextureBack; };
	UFUNCTION(BlueprintCallable, Category = "Pokemon")
	UTexture2D* getTextureFront() { return TextureFront; };
	UFUNCTION(BlueprintCallable, Category = "Pokemon")
	int32 getExp() { return Exp; };
	UFUNCTION(BlueprintCallable, Category = "Pokemon")
	int32 getMaxHP() { return MaxHP; };
	UFUNCTION(BlueprintCallable, Category = "Pokemon")
	int32 getCurHP() { return CurHP; };
	/*
	UFUNCTION(BlueprintCallable, Category = "Pokemon")
	FText getHPDisplayText() {
		// FString String = FString::Printf(TEXT("%d / %d"), CurHP, MaxHP);
		//return FText::FromString(String);
		return FText::FromString(Name);
	};
	*/
	UFUNCTION(BlueprintCallable, Category = "Pokemon")
	int32 getRarFactor() { return RarFactor; };
	UFUNCTION(BlueprintCallable, Category = "Pokemon")
	FString getRegion() { return Region; };

	// Attacks
	UFUNCTION(BlueprintCallable, Category = "Pokemon")
	TArray<UAttack*> GetAttacks() { return ListAttack; };
	// Returns null if no attack can be learned at this level
	UFUNCTION(BlueprintCallable, Category = "Pokemon")
	UAttack* GetAttackToLvl(int32 Lvl) { return *MapNameAttack.Find(Lvl); };

	// Pokedex data
	UFUNCTION(BlueprintCallable, Category = "Pokemon")
	float getHeight() { return Height; };
	UFUNCTION(BlueprintCallable, Category = "Pokemon")
	float getWeight() { return Weight; };
	UFUNCTION(BlueprintCallable, Category = "Pokemon")
	FString getDescription() { return Description; };

	// Evolution
	UFUNCTION(BlueprintCallable, Category = "Pokemon")
	bool CanEvolve() { return Evolve; };
	UFUNCTION(BlueprintCallable, Category = "Pokemon")
	int32 getEvolLvl() { return EvolLvl; };
	UFUNCTION(BlueprintCallable, Category = "Pokemon")
	int32 getNextEvolID() { return NextEvolID; };


	//////////////////////////////////////////////////////////////////////////
	////////// Setter

	// Basics
	void setID(int32 ID) { this->ID = ID; };
	void setName(FString Name) { this->Name = Name; }
	void setLvl(int32 Lvl) { this->Lvl = Lvl; };
	void setTextureBack(UTexture2D* TextureBack) { this->TextureBack = TextureBack; };
	void setTextureFront(UTexture2D* TextureFront) { this->TextureFront = TextureFront; };

	void setExp(int32 Exp) { this->Exp = Exp; };
	void setMaxHP(int32 MaxHP) { this->MaxHP = MaxHP; };
	void setCurHP(int32 CurHP) { this->CurHP = CurHP; };
	void setRarFactor(int32 RarFactor) { this->RarFactor = RarFactor; };
	void setRegion(FString Region) { this->Region = Region; };

	// Attacks
	void setListAttack(TArray<UAttack*> ListAttack) { this->ListAttack = ListAttack; };
	void setMapNameAttack(TMap<int32, UAttack*> MapNameAttack) { this->MapNameAttack = MapNameAttack; };

	// Pokedex data
	void setHeight(float Height) { this->Height = Height; };
	void setWeight(float Weight) { this->Weight = Weight; };
	void setDescription(FString Description) { this->Description = Description; };

	// Evolution
	void setEvolve(bool Evolve) { this->Evolve = Evolve; };
	void setEvolLvl(int32 EvolLvl) { this->EvolLvl = EvolLvl; };
	void setNextEvolID(int32 NextEvolID) { this->NextEvolID = NextEvolID; };



	// TODO
	// attacks
	// Check if ListAttacks == 4
	// if true then setAttacks(At1, At2) At1 is to be removed and At2 to be added
	// polymorph setAttacks(At);
	// or insert with ID and delete with ID
	
};
#undef LOCTEXT_NAMESPACE
// #endif