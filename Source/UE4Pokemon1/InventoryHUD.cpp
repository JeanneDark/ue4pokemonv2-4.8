// Fill out your copyright notice in the Description page of Project Settings.

#include "UE4Pokemon1.h"
#include "InventoryHUD.h"

AInventoryHUD::AInventoryHUD(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void AInventoryHUD::PostInitializeComponents()
{
	UE_LOG(LogTemp, Warning, TEXT("AInventoryHUD-------PostInitializeComponents-----"));

	Super::PostInitializeComponents();

	SAssignNew(InventoryWidget, SInventoryWidget).InventoryHUD(this);

	if (GEngine->IsValidLowLevel())
	{
		GEngine->GameViewport->AddViewportWidgetContent(SNew(SWeakWidget).PossiblyNullContent(InventoryWidget.ToSharedRef()));
	}
}


