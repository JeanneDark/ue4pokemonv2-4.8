// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "PokeActor.generated.h"

UCLASS()
class UE4POKEMON1_API APokeActor : public AActor
{
	GENERATED_BODY()

public:

	APokeActor(const FObjectInitializer& ObjectInitializer);

	// TODO
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Switch Components")
	class UBoxComponent* Box2;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Switch Components")
	class UPaperSprite* PokeballSprite;

	//UPROPERTY()
	// A UBillboardComponent to hold Icon sprite
	//UBillboardComponent* SpriteComponent;

	/*
	UFUNCTION(BlueprintNativeEvent, Category = "Switch Functions")
	void OnOverlapBegin(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION(BlueprintNativeEvent, Category = "Switch Functions")
	void OnOverlapEnd(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	*/
};
