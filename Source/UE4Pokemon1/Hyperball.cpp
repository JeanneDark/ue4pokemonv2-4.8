
#include "UE4Pokemon1.h"
#include "Hyperball.h"

Hyperball::Hyperball()
{
	maxVal = 200;
	price = 200;
	description = FString("Ein Ball mit hoher Erfolgsquote. Dem Superball in allen Punkten �berlegen.");
}

void Hyperball::accept(BaseItemVisitor &v)
{
	v.visit(*this);
}