
#pragma once
//#pragma message ("~~~~~~~~~~~~BaseBall object is defined")

#include "BaseItem.h"

class BaseBall : public BaseItem
{

protected:

	// TODO Verkaufswert adden
	int maxVal = 255;
	int price = 200;

public:

	BaseBall();

	int GetMaxVal() { return maxVal; }
	int GetPrice() { return price; }
	void accept(BaseItemVisitor &);
	
};

