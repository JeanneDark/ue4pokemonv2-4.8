// Fill out your copyright notice in the Description page of Project Settings.

#include "UE4Pokemon1.h"
#include "InventoryWidget.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SInventoryWidget::Construct(const FArguments& args)
{
	UE_LOG(LogTemp, Warning, TEXT("SInventoryWidget-------Construct-----"));

	InventoryHUD = args._InventoryHUD;

	ChildSlot
		[
			SNew(SOverlay)
			+ SOverlay::Slot()
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Top)
			[
				SNew(STextBlock)
				.ColorAndOpacity(FLinearColor::White)
				.ShadowColorAndOpacity(FLinearColor::Black)
				.ShadowOffset(FIntPoint(-1, 1))
				.Font(FSlateFontInfo("Arial", 26))
				.Text(FText::FromString("Main Menu"))
			]
			+ SOverlay::Slot()
				.HAlign(HAlign_Right)
				.VAlign(VAlign_Bottom)
				[
					SNew(SVerticalBox)
					+ SVerticalBox::Slot()
					[
						SNew(SButton)
						.Text(FText::FromString("Play Game!"))
						.OnClicked(this, &SInventoryWidget::PlayGameClicked)
					]
					+ SVerticalBox::Slot()
						[
							SNew(SButton)
							.Text(FText::FromString("Quit Game"))
							.OnClicked(this, &SInventoryWidget::QuitGameClicked)
						]
				]
		];

}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FReply SInventoryWidget::PlayGameClicked()
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Yellow, TEXT("PlayGameClicked"));
	}

	return FReply::Handled();
}

FReply SInventoryWidget::QuitGameClicked()
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Yellow, TEXT("QuitGameClicked"));
	}

	return FReply::Handled();
}