// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 * 
 */
class UE4POKEMON1_API Utility
{

public:

	Utility();

	//////////////////////////////////////////////////////////////////////////
	////////// Pace Utility
	////////// MAX_PACE	- is the MAX count of paces the player can walk in tall grass, water, ...
	////////// MIN_PACE	- is the MIN "

	static const int32 MAX_PACE = 10;
	static const int32 MIN_PACE = 2;

	static int32 GenerateRandomPaceCount();

	static int32 GenerateRandomNumberBtw0And3();
};
