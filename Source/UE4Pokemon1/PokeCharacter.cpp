// Fill out your copyright notice in the Description page of Project Settings.

#include "UE4Pokemon1.h"
#include "ItemManager.h"
#include "PokeCharacter.h"
#include "PokeInventory.h"
#include "ActionManager.h"
#include "PaperFlipbookComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogPokeCharacter, Log, All);

//static APokeInventory* PInventory = new APokeInventory();

APokeCharacter::APokeCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	SetupAssets();

	PActionManager = new ActionManager();
	PInventory = new PokeInventory();

	SetupDatabase();

	OptionMenuWidget = CreateWidget<UUserWidget>(GetWorld(), OptionMenuWidgetClass);
	BattleWidget = CreateWidget<UUserWidget>(GetWorld(), BattleWidgetClass);

	//GetSprite()->AddRelativeRotation(FRotator(0.0f, 0.0f, -90.0f));
	//GetSprite()->AddLocalRotation(FRotator(0.0f, 0.0f, -90.0f));

	GetSprite()->SetFlipbook(RunningDownAnimation);
	GetSprite()->SetFlipbook(RunningUpAnimation);
	GetSprite()->SetFlipbook(RunningRightAnimation);
	GetSprite()->SetFlipbook(RunningLeftAnimation);
	GetSprite()->SetFlipbook(IdleDownAnimation);
	GetSprite()->AddLocalRotation(FRotator(0.0f, 0.0f, -90.0f));

	GetCapsuleComponent()->SetCapsuleSize(5.80217f, 12.539499f, true);

	SetLastIdleState(IdleDownAnimation);

	// Create a camera boom attached to the root (capsule)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->AttachTo(RootComponent);
	CameraBoom->TargetArmLength = 0.0f;
	CameraBoom->SocketOffset = FVector(-90.0f, 0.0f, 0.0f);
	CameraBoom->bAbsoluteRotation = true;
	CameraBoom->RelativeRotation = FRotator(-90.0f, 0.0f, -90.0f);

	// Create an orthographic camera (no perspective) and attach it to the boom
	SideViewCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("SideViewCamera"));
	SideViewCameraComponent->ProjectionMode = ECameraProjectionMode::Orthographic;
	SideViewCameraComponent->OrthoWidth = 256.0f;
	SideViewCameraComponent->AttachTo(CameraBoom, USpringArmComponent::SocketName);

	// Prevent all automatic rotation behavior on the camera, character, and camera component
	CameraBoom->bAbsoluteRotation = true;
	SideViewCameraComponent->bUsePawnControlRotation = false;
	GetCharacterMovement()->bOrientRotationToMovement = false;

	// Behave like a traditional 2D platformer character, with a flat bottom instead of a curved capsule bottom
	// Note: This can cause a little floating when going up inclines; you can choose the tradeoff between better
	// behavior on the edge of a ledge versus inclines by setting this to true or false
	// GetCharacterMovement()->bUseFlatBaseForFloorChecks = true;

	// Enable replication on the Sprite component so animations show up when networked
	//GetSprite()->SetIsReplicated(true);
	//bReplicates = true;

	OnActorBeginOverlap.AddDynamic(this, &APokeCharacter::OnOverlapBegin);
	OnActorEndOverlap.AddDynamic(this, &APokeCharacter::OnOverlapEnd);

	SetGameMode(EGameModeEnum::Outside);
}

void APokeCharacter::SetupAssets()
{
	// Setup the assets
	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> RunningDownAnimationAsset;
		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> RunningUpAnimationAsset;
		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> RunningRightAnimationAsset;
		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> RunningLeftAnimationAsset;

		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> IdleDownAnimationAsset;
		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> IdleUpAnimationAsset;
		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> IdleRightAnimationAsset;
		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> IdleLeftAnimationAsset;

		ConstructorHelpers::FObjectFinderOptional<UClass> OptionMenuWidgetAsset;
		ConstructorHelpers::FObjectFinderOptional<UClass> BattleWidgetAsset;

		FConstructorStatics()
			: RunningDownAnimationAsset(TEXT("/Game/Asset/Animations/AshDown.AshDown"))
			, RunningUpAnimationAsset(TEXT("/Game/Asset/Animations/AshUp.AshUp"))
			, RunningRightAnimationAsset(TEXT("/Game/Asset/Animations/AshDown.AshDown"))
			, RunningLeftAnimationAsset(TEXT("/Game/Asset/Animations/AshUp.AshUp"))

			, IdleDownAnimationAsset(TEXT("/Game/Asset/Animations/AshDownIdle.AshDownIdle"))
			, IdleUpAnimationAsset(TEXT("/Game/Asset/Animations/AshUpIdle.AshUpIdle"))
			, IdleRightAnimationAsset(TEXT("/Game/Asset/Animations/AshRightIdle.AshRightIdle"))
			, IdleLeftAnimationAsset(TEXT("/Game/Asset/Animations/AshLeftIdle.AshLeftIdle"))

			, OptionMenuWidgetAsset(TEXT("/Game/Blueprints/Widgets/OptionMenuWidget.OptionMenuWidget_C"))
			, BattleWidgetAsset(TEXT("/Game/Blueprints/Widgets/BattleUI.BattleUI_C"))
		{
		}
	};
	static FConstructorStatics ConstructorStatics;

	RunningDownAnimation = ConstructorStatics.RunningDownAnimationAsset.Get();
	RunningUpAnimation = ConstructorStatics.RunningUpAnimationAsset.Get();
	RunningRightAnimation = ConstructorStatics.RunningRightAnimationAsset.Get();
	RunningLeftAnimation = ConstructorStatics.RunningLeftAnimationAsset.Get();

	IdleDownAnimation = ConstructorStatics.IdleDownAnimationAsset.Get();
	IdleUpAnimation = ConstructorStatics.IdleUpAnimationAsset.Get();
	IdleRightAnimation = ConstructorStatics.IdleRightAnimationAsset.Get();
	IdleLeftAnimation = ConstructorStatics.IdleLeftAnimationAsset.Get();

	OptionMenuWidgetClass = ConstructorStatics.OptionMenuWidgetAsset.Get();
	BattleWidgetClass = ConstructorStatics.BattleWidgetAsset.Get();
}

void APokeCharacter::SetupDatabase()
{
	UE_LOG(LogTemp, Warning, TEXT("APokeCharacter-------SetupDatabase"));

	// PManager = new UPokemonManager();
	// PManager = (UPokemonManager*) UPokemonManager::StaticClass()->GetDefaultObject(true);
	PManager = NewObject<UPokemonManager>();
	PManager->initialize();

	// UPokemonManager* PManager = (UPokemonManager*) NewObject<UPokemonManager>()
	// UPokemonManager* PManager = new UPokemonManager();
	// UPokemonManager::initialize();


	// Bild uergeben blueprint

	UE_LOG(LogTemp, Warning, TEXT("APokeCharacter-------SetupDatabase"));
}


//////////////////////////////////////////////////////////////////////////
// Animation

void APokeCharacter::UpdateAnimation(UPaperFlipbook* RunningAnimation, UPaperFlipbook* IdleAnimation)
{

	const FVector PlayerVelocity = GetVelocity();
	const float PlayerSpeed = PlayerVelocity.Size();

	// Are we moving or standing still?
	UPaperFlipbook* DesiredAnimation = (PlayerSpeed > 0.0f) ? RunningAnimation : IdleAnimation;

	SetLastIdleState(IdleAnimation);

	GetSprite()->SetFlipbook(DesiredAnimation);
	// GetSprite()->AddRelativeRotation(FRotator(0.0f, 0.0f, -90.0f));
}

void APokeCharacter::BeginPlay()
{
	Super::BeginPlay();

}

void APokeCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

	InputComponent->BindAxis("MoveX", this, &APokeCharacter::MoveHorizontal);
	InputComponent->BindAxis("MoveY", this, &APokeCharacter::MoveVertical);

	InputComponent->BindAction("AButton", IE_Pressed, this, &APokeCharacter::SetAButtonPressed);
	InputComponent->BindAction("BButton", IE_Pressed, this, &APokeCharacter::SetBButtonPressed);
	InputComponent->BindAction("OptionMenuAction", IE_Pressed, this, &APokeCharacter::OnAltButtonPressed);

}

void APokeCharacter::MoveHorizontal(float Value)
{
	UPaperFlipbook* RunningAnimation = RunningLeftAnimation;
	UPaperFlipbook* IdleAnimation = IdleLeftAnimation;

	// Set the rotation so that the character faces his direction of travel.
	if (Controller != nullptr)
	{
		if (Value < 0.0f)
		{
			// Controller->SetControlRotation(FRotator(0.0, 180.0f, 0.0f));
			RunningAnimation = RunningLeftAnimation;
			IdleAnimation = IdleLeftAnimation;

			//GetSprite()->SetFlipbook(DesiredAnimation);

			//GetSprite()->AddLocalRotation(FRotator(0.0f, 0.0f, -90.0f));
			//GetSprite()->AddRelativeRotation(FRotator(0.0f, 0.0f, -90.0f));


			UpdateAnimation(RunningAnimation, IdleAnimation);
		}
		else if (Value > 0.0f)
		{
			// Controller->SetControlRotation(FRotator(0.0f, 0.0f, 0.0f));
			RunningAnimation = RunningRightAnimation;
			IdleAnimation = IdleRightAnimation;
			//GetSprite()->SetFlipbook(DesiredAnimation);
			//GetSprite()->AddLocalRotation(FRotator(0.0f, 0.0f, -90.0f));

			//GetSprite()->AddLocalRotation(FRotator(0.0f, 0.0f, -90.0f));
			//GetSprite()->AddRelativeRotation(FRotator(0.0f, 0.0f, -90.0f));

			UpdateAnimation(RunningAnimation, IdleAnimation);
		}
		else
		{
			GetSprite()->SetFlipbook(GetLastIdleState());
			// UE_LOG(LogTemp, Warning, TEXT("MoveHorizontal - else"));

		}

	}

	// Update animation to match the motion
	// UpdateAnimation(RunningAnimation, IdleAnimation);

	//GetSprite()->AddRelativeRotation(FRotator(0.0f, 0.0f, -90.0f));

	// Apply the input to the character motion
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), Value);
}

void APokeCharacter::SetLastIdleState(UPaperFlipbook* IdleAnimation)
{
	CurrentIdleAnimation = IdleAnimation;

}

UPaperFlipbook* APokeCharacter::GetLastIdleState()
{
	return CurrentIdleAnimation;

}

void APokeCharacter::MoveVertical(float Value)
{
	//UPaperFlipbook* RunningAnimation = RunningLeftAnimation;
	UPaperFlipbook* RunningAnimation = RunningDownAnimation;
	//UPaperFlipbook* IdleAnimation = IdleLeftAnimation;
	UPaperFlipbook* IdleAnimation = IdleDownAnimation;

	// Set the rotation so that the character faces his direction of travel.
	if (Controller != nullptr)
	{
		if (Value < 0.0f)
		{
			// Controller->SetControlRotation(FRotator(180.0f, 0.0f, 0.0f));
			RunningAnimation = RunningUpAnimation;
			IdleAnimation = IdleUpAnimation;
			//GetSprite()->SetFlipbook(DesiredAnimation);
			//GetSprite()->AddRelativeRotation(FRotator(0.0f, 0.0f, -90.0f));

			UpdateAnimation(RunningAnimation, IdleAnimation);
		}
		else if (Value > 0.0f)
		{
			// Controller->SetControlRotation(FRotator(0.0f, 0.0f, 0.0f));
			RunningAnimation = RunningDownAnimation;
			IdleAnimation = IdleDownAnimation;
			//GetSprite()->SetFlipbook(DesiredAnimation);
			//GetSprite()->AddRelativeRotation(FRotator(0.0f, 0.0f, -90.0f));

			UpdateAnimation(RunningAnimation, IdleAnimation);
		}
		else
		{
			GetSprite()->SetFlipbook(GetLastIdleState());
			// UE_LOG(LogTemp, Warning, TEXT("MoveVertical - else"));

		}

	}

	// Apply the input to the character motion
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), Value);
}

void APokeCharacter::OnOverlapBegin(AActor* OtherActor)
{
	
	UE_LOG(LogTemp, Warning, TEXT("APokeCharacter-------OnOverlapBegin-----"));

	if (OtherActor)
	{
		USceneComponent* RootComponent = OtherActor->GetRootComponent();
		TArray<FName> Tags = RootComponent->ComponentTags;

		if (Tags.Num() >= 0)
		{

			check(Tags.Num() == 2);

			CheckForFight(Tags);
		}
	}

	SetIfOverlapping(true);
	SetOverlappingActor(OtherActor);

}

void APokeCharacter::OnOverlapEnd(AActor* OtherActor)
{
	//UE_LOG(LogTemp, Warning, TEXT("APokeCharacter-------OnOverlapEnd-----"));

	SetIfOverlapping(false);
	SetOverlappingActor(NULL);

}

void APokeCharacter::SetAButtonPressed()
{
	UE_LOG(LogTemp, Warning, TEXT("APokeCharacter-------SetAButtonPressed-----"));

	if (IsOverlapping())
	{
		AActor* Actor = GetOverlappingActor();
	
		UE_LOG(LogTemp, Warning, TEXT("APokeCharacter-------IsOverlapping-----"));

		if (Actor)
		{
			UE_LOG(LogTemp, Warning, TEXT("APokeCharacter-------Actor-----"));


			USceneComponent* RootComponent = Actor->GetRootComponent();
			TArray<FName> Tags = RootComponent->ComponentTags;

			//UE_LOG(Poke, Warning, TEXT("APokeCharacter - Type %s "), *Tags[0].ToString());

			// Component must have 2 Tags
			// First indicates the type e.g. Sign, PokeballItem
			// Second gives further information:
			// Sign -> "Sign_Level_1_N1" Key for database to lookup the text
			// PokeballItem -> "Meisterball"/ "Pokeball"/ ...
			if (Tags.Num() == 2)
			{
				FName tType = Tags[0];
				ETypeEnum Type = UItemManager::GetCorrespondingStringAsETypeEnum(tType.ToString());
				// UE_LOG(Poke, Warning, TEXT("APokeCharacter - Type %s "), *tType.ToString());
				
				FName tItem = Tags[1];
				//EItemEnum Item = UItemManager::GetCorrespondingStringAsEItemEnum(tItem.ToString());
				
				BaseItem* bItem = UItemManager::GetCorrespondingStringAsBaseItem(tItem.ToString());

				PActionManager->DoAction(Type, bItem, Actor, this);
				//PActionManager->DoAction(Type, Item, this);

			}

			

		}

	}

}

void APokeCharacter::SetBButtonPressed()
{
	UE_LOG(LogTemp, Warning, TEXT("APokeCharacter-------SetBButtonPressed-----"));
}

void APokeCharacter::OnAltButtonPressed()
{
	UE_LOG(LogTemp, Warning, TEXT("APokeCharacter-------OnAltButtonPressed-----"));
	
	if (WasAltButtonPressedBefore)
	{
		OptionMenuWidget->RemoveFromViewport();
	}
	else
	{
		// https://answers.unrealengine.com/questions/138594/focus-issue-when-adding-umg-widget-in-code.html?sort=oldest
		OptionMenuWidget->AddToViewport();
		FInputModeUIOnly Mode;
		Mode.SetWidgetToFocus(OptionMenuWidget->GetCachedWidget());
		//GetPlayerController()->SetInputMode(Mode);
		//GetPlayerContext().GetPlayerController()->SetInputMode(Mode);
		//SetInputMode(Mode);
		//bShowMouseCursor = true
	}
	
	WasAltButtonPressedBefore = !WasAltButtonPressedBefore;

	//pMyParentWidget->AddChild(pButtonWidget);

}

APlayerController* APokeCharacter::GetPlayerController()
{
	APlayerController* Controller = GetWorld()->GetFirstPlayerController();
	
	if (Controller)
		return Controller;
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("APokeCharacter-------GetPlayerController---NULL--"));
		return Controller;
	}
}

void APokeCharacter::SetIfOverlapping(const bool state)
{
	this->overlapping = state;

}

bool APokeCharacter::IsOverlapping()
{
	return this->overlapping;
}

void APokeCharacter::SetOverlappingActor(AActor* Actor)
{
	this->OverlappingActor = Actor;
}

AActor* APokeCharacter::GetOverlappingActor()
{
	return OverlappingActor;
}

PokeInventory* APokeCharacter::GetPokeInventory()
{
	return PInventory;
}

EGameModeEnum APokeCharacter::GetGameMode()
{
	return GameMode;
}

void APokeCharacter::SetGameMode(EGameModeEnum NewGameMode)
{
	GameMode = NewGameMode;
}

/*
TArray<BaseItem*> APokeCharacter::GetListItems()
{
	return new BaseItem();
	//return GetPokeInventory()->GetListItems();
}
*/

TArray<FString> APokeCharacter::GetListDisplayText()
{
	return GetPokeInventory()->GetListDisplayText();
}

void APokeCharacter::OnMenuEntryClicked(int32 ActionIndex)
{
	UE_LOG(LogTemp, Warning, TEXT("APokeCharacter-------OnMenuEntryClicked----"));

	//GetPokeInventory()->OnMenuEntryClicked(ActionIndex, OptionMenuWidget);
	GetPokeInventory()->OnMenuEntryClicked(ActionIndex);
}

void APokeCharacter::CheckForFight(TArray<FName> Tags)
{
	FString Type = *Tags[0].ToString();

	UE_LOG(Poke, Warning, TEXT("APokeCharacter - Type %s "), *Tags[0].ToString());

	// TODO water etc. new enum?
	if (Type.Compare(FString("TallGrass")) == 0)
	{
		if (++CurrenPaceCount >= RandomPaceCount)
		{
			StartFight(Type, *Tags[1].ToString());
			CurrenPaceCount = 0;
			RandomPaceCount = Utility::GenerateRandomPaceCount();
		}		
	}
}

// TODO Type determines the setting
// e.g. changes with water
void APokeCharacter::StartFight(FString Type, FString Region)
{
	UE_LOG(LogTemp, Warning, TEXT("APokeCharacter-------StartFight-----"));

	BattleWidget->AddToViewport();
	FInputModeUIOnly Mode;
	Mode.SetWidgetToFocus(BattleWidget->GetCachedWidget());

}
