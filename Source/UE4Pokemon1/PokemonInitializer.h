// Copyright (C) Timo Guehring <timo.guehring@googlemail.com>

#pragma once

#include "Pokemon.h"
// #include "Attack.h"


/**
* PokemonInitializer
* ~~~~~~~~~~~~~~~
* Implemented as Singleton to prevent several instances. The
* PokemonInitializer initializes all the Pokemon, attacks, persons, etc.
*
* The best design would have been to make everything static, so no instance
* would have been needed, but a static TArray doesn't work, so decided to
* implement it as a Singleton.
*
* Unfortunately SQLite didn't work in UE and I didn't want to operate with
* XML or Json files, so I decided to implement it this way.
*
*/
class UE4POKEMON1_API PokemonInitializer
{

private:
	PokemonInitializer();

	static PokemonInitializer *Instance;

	TArray<UPokemon*> ListPokemon;

	void initializePokemon();
	void setAttackAttributes(UAttack* Attack, int32 ID, FString Name, EType Type, int32 PP, int32 Power, int32 Accuracy);
	void setPokemonAttributes(UPokemon* Pokemon,
		int32 ID,
		FString Name,
		int32 Lvl,
		int32 MaxHP,
		int32 CurHP,
		int32 RarFactor,
		FString Region,
		TArray<UAttack*> ListAttack,
		TMap<int32, UAttack*> MapNameAttack,
		float Height,
		float Weight,
		FString Description,
		bool Evolve,
		int32 EvolLvl,
		int32 NextEvolID);
	

public:
	
	static PokemonInitializer* GetPokemonInitializer();

	TArray<UPokemon*> GetPokemon() { return ListPokemon; };

};
