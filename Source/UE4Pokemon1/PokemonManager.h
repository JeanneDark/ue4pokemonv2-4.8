// Copyright (C) Timo Guehring <timo.guehring@googlemail.com>

#pragma once

#include "PokemonInitializer.h"
#include "PaperSpriteComponent.h"
#include "PokemonManager.generated.h"

/**
 * 
 */
UCLASS()
class UPokemonManager : public UObject
{
	GENERATED_BODY()

	TArray<UPokemon*> ListPokemon;

public:
	UPokemonManager();

	void initialize();

	UFUNCTION(BlueprintCallable, Category = "Battle")
	UTexture2D* getOwnPokemonTexture();
	
	UFUNCTION(BlueprintCallable, Category = "Battle")
	UPokemon* getPokemonOwn();

	UFUNCTION(BlueprintCallable, Category = "Battle")
	UPokemon* getPokemonEnemy();

};
