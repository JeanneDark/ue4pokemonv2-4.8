#pragma once

#include "BaseItem.h"
//#include "BaseItemVisitor.h"

class BasePotion : public BaseItem
{

protected:

	// TODO add bool doesItHeal functions?
	int price = 200;
	int priceOfSale = 100;
	int countOfHP = 20;

public:

	BasePotion();

	int GetPrice() { return price; }
	int GetPriceOfSale() { return priceOfSale; }
	int GetCountOfHP() { return countOfHP; }
	void accept(BaseItemVisitor &);

};

