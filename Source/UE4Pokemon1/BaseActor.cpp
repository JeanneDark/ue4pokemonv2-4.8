// Copyright (C) Timo Guehring <timo.guehring@googlemail.com>

#include "UE4Pokemon1.h"
#include "BaseActor.h"



ABaseActor::ABaseActor(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Box2 = ObjectInitializer.CreateDefaultSubobject<UBoxComponent>(this, TEXT("BoxPokeball"));
	Flipbook = ObjectInitializer.CreateDefaultSubobject<UPaperFlipbookComponent>(this, TEXT("Flipbook"));

	MapDirectionAnimation.Add(FString("D"), IdleDownAnimation);
	MapDirectionAnimation.Add(FString("U"), IdleUpAnimation);
	MapDirectionAnimation.Add(FString("L"), IdleLeftAnimation);
	MapDirectionAnimation.Add(FString("R"), IdleRightAnimation);

}

/**
 * SetupActor has to be called after calling SetupAssets. SetupAssets is a pure virtual function and
 * has to be implemented in the deriving classes. SetupAssets has to be called in the Constructor
 * in the deriving classes first then SetupActor
 */
void ABaseActor::SetupActor()
{
	Box2->SetVisibility(true);
	Box2->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	Box2->SetCollisionProfileName(TEXT("BlockAll"));

	RootComponent = Box2;
	RootComponent->Mobility = EComponentMobility::Movable;

	Flipbook->SetFlipbook(IdleDownAnimation);
	Flipbook->SetVisibility(true);
	Flipbook->AttachTo(Box2);
}

TArray<FString> ABaseActor::GetActorTags()
{
	TArray<FName> Tags = Flipbook->ComponentTags;
	

	UE_LOG(Poke, Warning, TEXT("ABaseActor-------Tagnum: %d "), Tags.Num());
	UE_LOG(Poke, Warning, TEXT("ABaseActor-------Framerate: %f "), Flipbook->GetFlipbookFramerate());

	// check(Tags.Num() >= 2);

	if (Tags.Num() < 2)
		UE_LOG(LogTemp, Warning, TEXT("ABaseActor-------GetActorTags-No Tags"));

	TArray<FString> StringTags;

	for (int i = 0; i < Tags.Num(); i++)
		StringTags.Add(Tags[i].ToString());

	return StringTags;
}

/**
 * Must be called in BeginPlay, otherwise GetWorld or GetWorldTimeManager would be null
 * @param	TimeInterval	Declares the time interval to be repeated, optional, default 4.0f
 */
void ABaseActor::CreateTimer(const float TimeInterval)
{
	UWorld* World = GetWorld();

	if (World)
	{
		if (TimeHandler.IsValid() == false)
		{
			World->GetTimerManager().SetTimer(TimeHandler, this, &ABaseActor::PeriodicWalk, TimeInterval, true);
		}
		else
		{
			if (World->GetTimerManager().IsTimerPaused(TimeHandler) == true)
			{
				World->GetTimerManager().UnPauseTimer(TimeHandler);
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("ABaseActor-CreateTimer-Not Valid-"));
			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ABaseActor-CreateTimer-World NULL-"));
	}

}

void ABaseActor::PeriodicWalk()
{
	//UE_LOG(LogTemp, Warning, TEXT("ABaseActor----PeriodicWalk---"));

	float DiffHorizontal = 0.0f;
	float DiffVertical = 0.0f;
	UPaperFlipbook* DesiredAnimation = IdleDownAnimation;

	switch (Utility::GenerateRandomNumberBtw0And3())
	{
		// Right
		case 0:
			DiffHorizontal = 10.0f;
			DesiredAnimation = IdleRightAnimation;
			break;
		// Left
		case 1:
			DiffHorizontal = -10.0f;
			DesiredAnimation = IdleLeftAnimation;
			break;
		// Up
		case 2:
			DiffVertical = -10.0f;
			DesiredAnimation = IdleUpAnimation;
			break;
		// Down
		case 3:
			DiffVertical = 10.0f;
			DesiredAnimation = IdleDownAnimation;
			break;
		default:
			UE_LOG(LogTemp, Warning, TEXT("ABaseActor-PeriodicWalk-Switch default"));
			break;

	}

	Flipbook->SetFlipbook(DesiredAnimation);

	// FIX true/false wrong behavior
	//RootComponent->MoveComponent(FVector(0.0f + DiffHorizontal, 0.0f + DiffVertical, 0.0f), GetActorRotation(), true);

	Direction = FVector(DiffHorizontal, DiffVertical, 0.0f);

	RootComponent->MoveComponent(Direction, GetActorRotation(), false);

}

// TODO maybe setlocation() in beginplay()
void ABaseActor::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(LogTemp, Warning, TEXT("ABaseActor----BeginPlay---"));

	// Unable to set values in the constructor
	Flipbook->SetRelativeRotation(FRotator(0.0f, 0.0f, -90.0f));
	Box2->SetBoxExtent(FVector(5.0f, 6.0f, 40.0f));
}

void ABaseActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	IsHittingCharacter();
}

/**
 * Does a Raycast to check if the player walks in sight
 * @param	Distance	Declares the Distance from this actor to the player, optional, default is 200
 */
bool ABaseActor::IsHittingCharacter(const int Distance)
{
	const FVector StartTrace = GetActorLocation();
	const FVector EndTrace = StartTrace + Direction * Distance;

	FCollisionQueryParams TraceParams(FName(TEXT("CharacterTrace")), true, this);
	TraceParams.bTraceAsyncScene = true;

	FHitResult Hit(ForceInit);

	GetWorld()->LineTraceSingle(Hit, StartTrace, EndTrace, ECollisionChannel::ECC_PhysicsBody, TraceParams);

	APokeCharacter* Ash = Cast<APokeCharacter>(Hit.GetActor());

	if (Ash)
	{
		UE_LOG(LogTemp, Warning, TEXT("ABaseActor-------Trace true----"));
		return true;
	}
	
	return false;
}

