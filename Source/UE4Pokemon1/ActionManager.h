// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ItemManager.h"
#include "PokeCharacter.h"
#include "BaseItem.h"

/**
 * 
 */
class UE4POKEMON1_API ActionManager
{
public:
	ActionManager();

	//void DoAction(const ETypeEnum Type, const EItemEnum Item, class AActor* Actor, class APokeCharacter* Character);
	void DoAction(const ETypeEnum Type, BaseItem* Item, class AActor* Actor, class APokeCharacter* Character);

};
