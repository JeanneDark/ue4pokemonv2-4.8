
#pragma once
//#pragma message ("~~~~~~~~~~~~BaseItem object is defined")

#include "BaseItemVisitor.h"

class BaseItem
{

protected:

	FString description = FString("BaseItem description");

public:

	BaseItem();

	FString GetDescription() { return description; }
	virtual void accept(BaseItemVisitor &) = 0;

};

