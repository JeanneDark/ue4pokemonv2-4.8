// Copyright (C) Timo Guehring <timo.guehring@googlemail.com>

#pragma once

#include "GameFramework/Actor.h"
#include "PaperSpriteComponent.h"
#include "PaperFlipbookComponent.h"
#include "PaperFlipbook.h"
#include "Utility.h"
#include "PokeCharacter.h"
#include "BaseActor.generated.h"

UCLASS(abstract)
class UE4POKEMON1_API ABaseActor : public AActor
{
	GENERATED_BODY()

	FTimerHandle TimeHandler;

protected:
	// The animation to play while running around
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	class UPaperFlipbook* RunningDownAnimation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	class UPaperFlipbook* RunningUpAnimation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	class UPaperFlipbook* RunningLeftAnimation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	class UPaperFlipbook* RunningRightAnimation;

	// The animation to play while idle (standing still)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	class UPaperFlipbook* IdleDownAnimation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	class UPaperFlipbook* IdleUpAnimation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	class UPaperFlipbook* IdleLeftAnimation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	class UPaperFlipbook* IdleRightAnimation;

	FVector Direction = FVector(0.0f, 0.0f, 0.0f);

	TMap<FString, UPaperFlipbook*> MapDirectionAnimation;

	bool IsHittingCharacter(const int32 Distance = 200);

	virtual void SetupAssets() PURE_VIRTUAL(ABaseActor::SetupAssets, ;);
	virtual void SetupActor();

	virtual TArray<FString> GetActorTags();

	virtual void CreateTimer(const float TimeInterval = 4.0f);
	virtual void PeriodicWalk();

public:

	ABaseActor(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Switch Components")
	class UBoxComponent* Box2;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Switch Components")
	class UPaperFlipbook* CurrentIdleAnimation;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Switch Components")
	class UPaperFlipbookComponent* Flipbook;

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

};
