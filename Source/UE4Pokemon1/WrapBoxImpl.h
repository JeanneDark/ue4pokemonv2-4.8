// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Runtime/UMG/Public/Components/WrapBox.h"
#include "WrapBoxImpl.generated.h"

/**
 * 
 */
UCLASS()
class UE4POKEMON1_API UWrapBoxImpl : public UWrapBox
{
	GENERATED_BODY()

public:
	TSharedPtr<class SWrapBox> GetMyWrapBox();

};
