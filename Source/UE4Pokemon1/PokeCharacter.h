// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaperCharacter.h"
#include "ActionManager.h"
#include "PokeInventory.h"
#include "PokemonManager.h"
//#include "BaseItem.h"
#include "Utility.h"
#include "GameFramework/Character.h"
#include "PokeCharacter.generated.h"

UENUM(BlueprintType)
enum class EGameModeEnum : uint8
{
	Outside 		UMETA(DisplayName = "Outside"), // default
	Fight 			UMETA(DisplayName = "Fight"),
	Building		UMETA(DisplayName = "Building"),
	Cave			UMETA(DisplayName = "Cave")

};

UCLASS()
class UE4POKEMON1_API APokeCharacter : public APaperCharacter
{
	GENERATED_BODY()

	/** Side view camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* SideViewCameraComponent;

	/** Camera boom positioning the camera beside the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;


private:
	bool overlapping = false;

	class AActor* OverlappingActor;

	void SetIfOverlapping(bool state);

	bool IsOverlapping();

	void SetOverlappingActor(AActor* Actor);

	void SetupAssets();

	void SetupDatabase();

	AActor* GetOverlappingActor();

	//UPROPERTY()
	class ActionManager* PActionManager;


protected:
	// The animation to play while running around
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	class UPaperFlipbook* RunningDownAnimation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	class UPaperFlipbook* RunningUpAnimation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	class UPaperFlipbook* RunningLeftAnimation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	class UPaperFlipbook* RunningRightAnimation;

	// The animation to play while idle (standing still)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	class UPaperFlipbook* IdleDownAnimation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	class UPaperFlipbook* IdleUpAnimation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	class UPaperFlipbook* IdleLeftAnimation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	class UPaperFlipbook* IdleRightAnimation;

	class UPaperFlipbook* CurrentIdleAnimation;

	//////////////////////////////////////////////////////////////////////////
	////////// Widgets

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	//class UClass* InventoryWidgetClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	class UClass* OptionMenuWidgetClass;

	class UUserWidget* OptionMenuWidget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	class UClass* BattleWidgetClass;

	class UUserWidget* BattleWidget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PokemonManager)
	class UPokemonManager* PManager;

	EGameModeEnum GameMode;

	PokeInventory* PInventory;


	/** Called to choose the correct animation to play based on the character's movement state */
	void UpdateAnimation(UPaperFlipbook* RunningAnimation, UPaperFlipbook* IdleAnimation);

	void SetLastIdleState(UPaperFlipbook* IdleAnimation);
	class UPaperFlipbook* GetLastIdleState();

	/** Called for side to side input */
	void MoveHorizontal(float Value);
	void MoveVertical(float Value);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;


public:
	APokeCharacter(const FObjectInitializer& ObjectInitializer);

	/** Returns SideViewCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetSideViewCameraComponent() const { return SideViewCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	UFUNCTION()
	virtual void OnOverlapBegin(AActor* OtherActor);

	UFUNCTION()
	virtual void OnOverlapEnd(AActor* OtherActor);

	UFUNCTION()
	virtual void SetAButtonPressed();

	UFUNCTION()
	virtual void SetBButtonPressed();

	UFUNCTION()
	virtual void OnAltButtonPressed();

	UFUNCTION(BlueprintCallable, Category = "Battle")
	virtual UPokemonManager* getPokemonManager() { return PManager; };

	//////////////////////////////////////////////////////////////////////////
	////////// Battle

	//UFUNCTION(BlueprintCallable, Category = "Fight")
	void StartFight(FString Type, FString Region);

	void CheckForFight(TArray<FName> Tags);

	// TODO private
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	int32 CurrenPaceCount = 0;

	///UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	int32 RandomPaceCount = Utility::GenerateRandomPaceCount();

	//////////////////////////////////////////////////////////////////////////
	////////// GamdeMode

	UFUNCTION(BlueprintCallable, Category = "GameMode")
	virtual EGameModeEnum GetGameMode();

	UFUNCTION(BlueprintCallable, Category = "GameMode")
	virtual void SetGameMode(EGameModeEnum NewGameMode);

	bool WasAltButtonPressedBefore = false;

	APlayerController* GetPlayerController();

	//UFUNCTION()
	PokeInventory* GetPokeInventory();

	//UFUNCTION(BlueprintCallable, Category = "Inventory")
	//virtual TArray<BaseItem*> GetListItems();

	UFUNCTION(BlueprintCallable, Category = "Inventory")
	virtual TArray<FString> GetListDisplayText();

	UFUNCTION(BlueprintCallable, Category = "Inventory")
	virtual void OnMenuEntryClicked(int32 ActionIndex);

};
