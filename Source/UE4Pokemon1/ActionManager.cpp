// Fill out your copyright notice in the Description page of Project Settings.

#include "UE4Pokemon1.h"
#include "ActionManager.h"
#include "ItemManager.h"
#include "PokeCharacter.h"

/**
 Insgesamt das Design ueberdenken
 PokeCharacter ruft ActionManager auf, aber gibt seine eigene Ref weiter
 */

ActionManager::ActionManager()
{
}

// TODO Look for a more elegant way
void ActionManager::DoAction(const ETypeEnum Type, BaseItem* Item, class AActor* Actor, class APokeCharacter* Character)
{
	switch (Type)
	{
	case (ETypeEnum::PokeballItem) :
		//UE_LOG(LogTemp, Warning, TEXT("ActionManager-------PokeballItem-----"));
		if (Item)
			Character->GetPokeInventory()->AddToInventory(Item, Actor);
		break;
	case (ETypeEnum::Sign) :
		UE_LOG(LogTemp, Warning, TEXT("ActionManager-------Sign-----"));
		break;
	case (ETypeEnum::Door) :
		break;
	case (ETypeEnum::None) :
		UE_LOG(LogTemp, Warning, TEXT("ActionManager-------None-----"));
		break;
	default:
		UE_LOG(LogTemp, Warning, TEXT("ActionManager-------Default-----"));
		break;
	}

}

/*
// TODO Look for a more elegant way
void ActionManager::DoAction(const ETypeEnum Type, const EItemEnum Item, class APokeCharacter* Character)
{
	switch (Type)
	{
		case (ETypeEnum::PokeballItem):
			UE_LOG(LogTemp, Warning, TEXT("ActionManager-------PokeballItem-----"));
			break;
		case (ETypeEnum::Sign):
			UE_LOG(LogTemp, Warning, TEXT("ActionManager-------Sign-----"));
			break;
		case (ETypeEnum::Door):
			break;
		case (ETypeEnum::None):
			UE_LOG(LogTemp, Warning, TEXT("ActionManager-------None-----"));
			break;
		default:
			UE_LOG(LogTemp, Warning, TEXT("ActionManager-------Default-----"));
			break;
	}
	
}
*/
