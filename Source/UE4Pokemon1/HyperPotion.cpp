
#include "UE4Pokemon1.h"
#include "HyperPotion.h"


HyperPotion::HyperPotion()
{
	price = 1200;
	priceOfSale = 600;
	countOfHP = 200;
	description = FString("Hypertrank");
}

void HyperPotion::accept(BaseItemVisitor &v)
{
	v.visit(*this);
}